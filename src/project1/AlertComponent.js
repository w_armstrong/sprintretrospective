import React from "react";
import { MuiThemeProvider } from "@material-ui/core/styles";
import { Card,
  CardHeader,
  CardContent,
  TableContainer,
  Table,
  TableBody,
  TableCell,
  TableRow,
  Paper,
  TextField,} from "@material-ui/core";
import {TextInput, View, StyleSheet, Button, Text, } from 'react-native';
import theme from "./theme";
import "../App.css";
import Autocomplete from "@material-ui/lab/Autocomplete";
import Select from 'react-select';
import logo from "../images/logo.png";

import MemberData from './projects.json';
import ProjectData from './members.json';

class AlertComponent extends React.Component {
  constructor(props){
    super(props);

    this.state = {
      name: '',
      description: '',
      startDate: '',
      endDate: '',
      members: [],
      membersInput: [],
      currentSelected: null,
      currentMembersInProject: [],
      listMembers: [],
      value: "",
      valueProject: "",
      boolShow: false,
      projects: [],
      story: [],
    }
    
  } 

  render(){

    let rowIndex = 1;

    const finish = (project) =>{
      
      {MemberData.map((value, index) =>{
        if(value.projectName === project){
          this.state.name = (value.projectName);
          this.state.description = (value.desc);
          this.state.startDate = (value.startDate);
          this.state.endDate = (value.endDate);
          this.state.membersInput = (value.members);
        }
      })}

      console.log(this.state.name);
      console.log(this.state.description);
      console.log(this.state.startDate);
      console.log(this.state.endDate);
      console.log(this.state.membersInput);

      this.setState({boolShow: true});
    }

    const updateInfo = (project) =>{
      {MemberData.map((value, index) =>{
        if(value.projectName === project){
          (value.projectName) = this.state.name;
          (value.desc) = this.state.description;
          (value.startDate) = this.state.startDate;
          (value.endDate) = this.state.endDate;
          (value.members) = this.state.membersInput;
        }
      })}

      console.log(this.state.name);
      console.log(this.state.description);
      console.log(this.state.startDate);
      console.log(this.state.endDate);
      console.log(this.state.membersInput);

      this.setState({boolShow: true});
    }

    const allMembers = () =>{
      this.state.members = [];

      {MemberData.map((value, index) =>{
        if(value.members.includes(this.state.valueProject)){
          this.state.members.push(value.projectName);
        }
      })}

      console.log(this.state.members);
    }

    const allProjects = () =>{
      this.state.projects = [];

      {ProjectData.map((value, index) =>{
        this.state.projects.push(value.name);
      })}

      console.log(this.state.projects);
    }

    const doUpdate = (e) =>{
      this.setState({okay: 'd'});
    }

    const loadStories = () =>{
      this.state.story = []

      {MemberData.map((value, index) =>{
        if(value.story.user.includes(this.state.valueProject)){
          this.state.story.push(value.story.notes);
        }
      })}

      console.log(this.state.story);

      this.setState({boolShow: true});
    }

  return (
    <MuiThemeProvider theme={theme}>
      <Card>
        <h1
              style={{
                textAlign: "center",
                color: "purple"
              }}
            >
              Backlog - User Stories
            </h1>
        
        <View style = {{alignItems: "center", marginTop: 50, marginBottom: 15}}>

          <View style={{marginBottom: 50, marginLeft: 30}}>

            <View>{allProjects()}</View>
              <p>Select a User</p>
              <Autocomplete
              id="membersList"
              options={this.state.projects}
              onChange={(e) => this.setState({valueProject: e.target.innerHTML})}
              getOptionLabel={option => option}
              style={{ width: 290, paddingLeft: 30}}
              renderInput={params => (
                <TextField
                  {...params}
                  style={{color: "purple"}}
                  label="options"
                  variant="outlined"
                  fullWidth
                />
              )}
            />
          </View>

        <View style={{marginBottom: 50, marginLeft: 30}}>

            <View>{allMembers()}</View>
              <p>Select a Project</p>
              <Autocomplete
              id="membersList"
              options={this.state.members}
              onChange={(e) => this.setState({value: e.target.innerHTML})}
              getOptionLabel={option => option}
              style={{ width: 290, paddingLeft: 30}}
              renderInput={params => (
                <TextField
                  {...params}
                  style={{color: "purple"}}
                  label="options"
                  variant="outlined"
                  fullWidth
                />
              )}
            />
          </View>

          {this.state.boolShow ? (  
            <h2>Notes: </h2>
          ) : null}

          {this.state.boolShow ? (  
            <h2>{this.state.story}</h2>
          ) : null}
          
          <button onClick ={(e) => loadStories()}>Load Stories</button>
          
        </View>

      </Card>
    </MuiThemeProvider>
  );
  }
};

const styles = StyleSheet.create({
  input:{
    borderWidth: 1,
  },
});
export default AlertComponent;
