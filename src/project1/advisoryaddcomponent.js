import React from "react";
import { MuiThemeProvider } from "@material-ui/core/styles";
import { Card,
  CardHeader,
  CardContent,
  TableContainer,
  Table,
  TableBody,
  TableCell,
  TableRow,
  Paper,
  TextField,} from "@material-ui/core";
import {TextInput, View, StyleSheet, Button, Text, } from 'react-native';
import theme from "./theme";
import "../App.css";
import Autocomplete from "@material-ui/lab/Autocomplete";
import Select from 'react-select';
import logo from "../images/logo.png";

import MemberData from './members.json';

class Add extends React.Component {
  constructor(props){
    super(props);

    this.state = {
      name: '',
      desc: '',
      startDate: '',
      okay: '',
      currentSelected: null,
      members: [],
      currentMembersInProject: [],
      listMembers: [],
    }
    
  } 

  render(){

    let rowIndex = 1;

    const finish = () =>{
      var isName = false;
      var isDesc = false;
      var isDate = false;
      var isMembers = false;

      if(this.state.name.length > 0){
        isName = true;
      }

      if(this.state.desc.length > 0){
        isDesc = true;
      }

      if(this.state.startDate.match(/^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/)){
        isDate = true;
      }

      if(this.state.currentMembersInProject.length > 0){
        isMembers = true;
      }

      if(isName === true && isDesc === true && isDate === true && isMembers === true){
        console.log("Worked");

        const data = require('./projects.json');

        const newdata ={
          
          "projectName": `${this.state.name}`,
          "desc": `${this.state.desc}`,
          "startDate": `${this.state.startDate}`,
          "endDate": "",
          "members": [`${this.state.currentMembersInProject}`]
        }
        data.push(newdata);
        
        console.log(data);

        document.getElementById("field1").value = "";
        document.getElementById("field2").value = "";
        document.getElementById("field3").value = "";
        document.getElementById("membersList").value = "";
        this.state.currentMembersInProject = [];

      }
    }

    const allMembers = () =>{
      this.state.members = [];

      {MemberData.map((value, index) =>{
        this.state.members.push(value.name);
      })}

      console.log(this.state.members);
    }

    const addMember = (e) =>{
      this.state.currentMembersInProject.push(e);
      updateList();
    }

    const updateList = () =>{
      let showList = [];

      showList.push(<TableContainer disabled={false} component={Paper}>
            <Table size="small" aria-label="a dense table">
              <TableBody>
                {this.state.currentMembersInProject.map(row => (
                  <TableRow key={rowIndex++}>
                    <TableCell style={{color: "purple", fontSize: 20, textAlign: "center", padding: 10}}>{row}</TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>)

      return showList;
    }

    const doUpdate = (e) =>{
      this.setState({okay: 'd'});
    }

  return (
    <MuiThemeProvider theme={theme}>
      <Card>
        <h1
              style={{
                textAlign: "center",
                color: "purple"
              }}
            >
              New Project
            </h1>
        
        <View style = {{alignItems: "center", marginTop: 50, marginBottom: 15}}>
          <p>Name of Project</p>
          <TextInput id="field1" style = {styles.input} 
            onChangeText={(text) => this.setState({name:text})}
          ></TextInput>

          <p>Description</p>
          <TextInput id="field2" style = {styles.input} 
            onChangeText={(text) => this.setState({desc:text})}
          ></TextInput>

          <p>Start Date of Project (DD/MM/YYYY) </p>
          <TextInput id="field3" style = {styles.input} 
            onChangeText={(text) => this.setState({startDate:text})}
          ></TextInput>

          <View>
            {false ? (
          <TextInput style = {styles.input}  
            onChangeText={(text) => this.setState({okay:text})}
          ></TextInput>
            ) : null}
          </View>

          <View style={{marginBottom: 50, marginLeft: 30}}>
            <View>{allMembers()}</View>
              <p>Add Project Members (Atleast one member must be assigned) </p>
              <Autocomplete
              id="membersList"
              options={this.state.members}
              onChange={(e) => addMember(e.target.innerHTML)}
              getOptionLabel={option => option}
              style={{ width: 290, paddingLeft: 30}}
              renderInput={params => (
                <TextField
                  {...params}
                  style={{color: "purple"}}
                  label="options"
                  variant="outlined"
                  fullWidth
                />
              )}
            />
          </View>
          <p>Current Members:</p>
          {updateList()}
          <button onClick= {(e)=> doUpdate()}>Update Member List</button>
        </View>

            
      <View style={{marginBottom: 20, alignItems: "center"}}>
            <button  onClick = {(e) => finish()} >Create New Project</button>
      </View>

      </Card>
    </MuiThemeProvider>
  );
  }
};

const styles = StyleSheet.create({
  input:{
    borderWidth: 1,
  },
});
export default Add;
