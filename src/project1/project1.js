import React, { useState, useReducer } from "react";
import { MuiThemeProvider } from "@material-ui/core/styles";
import {
  Typography,
  AppBar,
  MenuItem,
  IconButton,
  Toolbar,
  Menu,
  Snackbar
} from "@material-ui/core";
import Reorder from "@material-ui/icons/Reorder";
import HomeComponent from "./HomeComponent";
import { Route, Link, Redirect } from "react-router-dom";
import AdvisoryListComponent from "./advisorylistcomponent";
import AdvisoryAddComponent from "./advisoryaddcomponent";

import theme from "./theme";
import "../App.css";
import AlertComponent from "./AlertComponent";

const Project1Component = () => {
  const initialState = {
    snackBarMsg: "",
    contactServer: false
  };

  const snackbarHandler = (e, contactServer) => {setState({ snackBarMsg: e, contactServer: true });};
  const snackbarClose = () => {setState({ contactServer: false }); };

  const [anchorEl, setAnchorEl] = useState(null);
  const reducer = (state, newState) => ({ ...state, ...newState });
  const [state, setState] = useReducer(reducer, initialState);

  //closes the anchor when a menu option is selected
  const handleClose = () => {setAnchorEl(null);};

  //drops down the options for screens to switch to
  const handleClick = event => {setAnchorEl(event.currentTarget); };

  return (
    <MuiThemeProvider theme={theme}>
      <AppBar position="static">

        <Toolbar>

          <Typography variant="h5" color="inherit">
            Team ABC - Sprint
          </Typography>

          <IconButton
            onClick={handleClick} //show options
            color="inherit"
            style={{ marginLeft: "auto", paddingRight: "1vh" }}
          >
            
            <Reorder />

          </IconButton>

          <Menu
            id="simple-menu"
            anchorEl={anchorEl}
            open={Boolean(anchorEl)}
            onClose={handleClose}
          >

      
            <MenuItem component={Link} to="/home" onClick={handleClose}
            
            //all the menuitems
            //Depending which menu item is pressed it will change the link
            
            >
              Register
            </MenuItem>

            <MenuItem component={Link} to="/add" onClick={handleClose}>
              New Project
            </MenuItem>

            <MenuItem component={Link} to="/list" onClick={handleClose}>
              Current Projects
            </MenuItem>

            <MenuItem component={Link} to="/alerts" onClick={handleClose}>
              Backlog
            </MenuItem>

          </Menu>

        </Toolbar>
      </AppBar>

      <div>

        <Route exact path="/" render={() => <Redirect to="/home" />} />
        <Route
          path="/alerts"
          render={() => <AlertComponent snackbarHandler={snackbarHandler} />}
        />

        <Route
          path="/add"
          render={() => (
            <AdvisoryAddComponent snackbarHandler={snackbarHandler} />
          )}
        />

        <Route
          path="/list"
          render={() => (
            <AdvisoryListComponent snackbarHandler={snackbarHandler} />
          )}
        />

        <Route path="/home" component={HomeComponent} />

      </div>

      <Snackbar
        open={state.contactServer}
        message={state.snackBarMsg}
        autoHideDuration={2500}
        onClose={snackbarClose}
      />

    </MuiThemeProvider>
  );
};

export default Project1Component;
