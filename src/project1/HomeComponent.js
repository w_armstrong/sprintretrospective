import React, {Component} from "react";
import { MuiThemeProvider } from "@material-ui/core/styles";
import { Card, CardContent, CardHeader, Typography, TextField, } from "@material-ui/core";
import {TextInput, View, StyleSheet, Button, Text} from 'react-native';
import theme from "./theme";
import "../App.css";

import logo from "../images/logo.png";

import MemberData from './members.json';

class HomeComponent extends React.Component {
  constructor(props){
    super(props);

    this.state = {
      first: '',
      last: '',
      email: '',
      password: '',
      confrimPassword: '',
    }
    
  } 

  render(){

    const finish = () =>{
      var isName = false;
      var isPassword = false;
      var isEmail = false;

      if(this.state.first.length > 0 && this.state.last.length > 0){
        isName = true;
      }

      if(this.state.password === this.state.confrimPassword && this.state.password.length >= 8){
        isPassword = true;
      }

      if(this.state.email.includes('@ABC.com') === true){
        isEmail = true;
      }

      if(isName === true && isPassword === true && isEmail === true){

        console.log("Worked");

        const data = require('./members.json');

        const newdata ={
          "first": `${this.state.first}`,
          "last": `${this.state.last}`,
          "name": `${this.state.first + " " + this.state.last}`,
          "email": `${this.state.email}`,
          "password": `${this.state.password}`
        }
        data.push(newdata);

        console.log(data);

        document.getElementById("field1").value = "";
        document.getElementById("field2").value = "";
        document.getElementById("field3").value = "";
        document.getElementById("field4").value = "";
        document.getElementById("field5").value = "";

      }
    }

  return (
    <MuiThemeProvider theme={theme}>
      <Card>
        <h1
              style={{
                textAlign: "center",
                color: "purple"
              }}
            >
              Register New Member
            </h1>
        
        <View style = {{alignItems: "center", marginTop: 15, marginBottom: 15}}>
          <p>First Name</p>
          <TextInput id="field1" style = {styles.input} 
            onChangeText={(text) => this.setState({first:text})}
          ></TextInput>

          <p>Last Name</p>
          <TextInput id="field2" style = {styles.input} 
            onChangeText={(text) => this.setState({last:text})}
          ></TextInput>

          <p>Email (requires @ABC.com)</p>
          <TextInput id="field3" style = {styles.input} 
            onChangeText={(text) => this.setState({email:text})}
          ></TextInput>

          <p>Password (Must be 8 Characters or more)</p>
          <TextInput id="field4" style = {styles.input} 
            onChangeText={(text) => this.setState({password:text})}
          ></TextInput>

          <p>Confrim Password</p>
          <TextInput id="field5" style = {styles.input} 
            onChangeText={(text) => this.setState({confrimPassword:text})}
          ></TextInput>
        </View>
              
      <View style={{marginBottom: 10, alignItems: "center"}}>
            <button  onClick = {(e) => finish()} >Finish</button>
      </View>

      </Card>
    </MuiThemeProvider>
  );
  }
};

const styles = StyleSheet.create({
  input:{
    borderWidth: 1,
  }
});
export default HomeComponent;
