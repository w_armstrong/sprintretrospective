import { createMuiTheme } from "@material-ui/core/styles";
export default createMuiTheme({
  typography: {
    useNextVariants: true
  },
    palette: {
      common: { black: "#000", white: "#fff" },
      background: { paper: "#fff", default: "#fafafa" },
      primary: {
        light: "#cb79b1",
        main: "#b53f90",
        dark: "#9f307c",
        contrastText: "#fff"
      },
      secondary: {
        light: "rgba(74, 74, 74, 1)",
        main: "rgba(0, 0, 0, 1)",
        dark: "rgba(155, 155, 155, 1)",
        contrastText: "#fff"
      },
      error: {
        light: "#e57373",
        main: "#f44336",
        dark: "#d32f2f",
        contrastText: "#fff"
      },
      text: {
        primary: "rgba(0, 0, 0, 0.87)",
        secondary: "rgba(0, 0, 0, 0.54)",
        disabled: "rgba(0, 0, 0, 0.38)",
        hint: "rgba(0, 0, 0, 0.38)"
      }
    
  }
});
