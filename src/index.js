import React from "react";
import { render } from "react-dom";
import App from "./project1/project1";
import { BrowserRouter as Router } from "react-router-dom";
render(
  <Router>
    <App />
  </Router>,
  document.querySelector("#root")
);
